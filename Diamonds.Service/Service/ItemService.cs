﻿using Diamonds.Service.Model.DB;
using Diamonds.Service.Model.Dto;
using Diamonds.Service.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Diamonds.Service.Service
{
    public class ItemService
    {
        private readonly ItemRepository _itemRepository;

        public ItemService(ItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public IEnumerable<ItemDto> FetchItemsInfo()
        {
            IEnumerable<Items> items = _itemRepository.GetItems();
            List<ItemDto> itemsDto = new List<ItemDto>(0);
            foreach (Items item in items)
            {
                IDictionary<string, List<ItemPhotoDto>> photos = GetItemPhotosByItemId(item.Id);
                ItemDto itemDto = new ItemDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    Photos = photos
                };
                itemsDto.Add(itemDto);
            }
            return itemsDto;
        }

        public IDictionary<string, List<ItemPhotoDto>> GetItemPhotosByItemId(int itemId)
        {
            IEnumerable<ItemPhotos> photos = _itemRepository.GetItemPhotosByItemId(itemId);
            IDictionary<string, List<ItemPhotoDto>> toReturn = new Dictionary<string, List<ItemPhotoDto>>(0);
            if (photos.Any())
            {
                foreach (ItemPhotos photo in photos)
                {
                    string props = GetPhotoProperties(photo.Id);
                    ItemPhotoDto photoDto = new ItemPhotoDto
                    {
                        ImageAlt = photo.Alt,
                        ImageName = photo.FileName,
                        Position = photo.Position.Value,
                        Type = photo.TypeId == 1 ? "Photo" : (photo.TypeId == 2 ? "Thumb" : string.Empty)
                    };
                    if (toReturn.ContainsKey(props))
                    {
                        toReturn[props].Add(photoDto);
                    }
                    else
                    {
                        toReturn.Add(props, new List<ItemPhotoDto>() { photoDto });
                    }
                }
            }
            return toReturn;
        }

        public bool IsPhotoNameUnique(string photoName)
        {
            return _itemRepository.PhotoNameUnique(photoName);
        }

        public ItemDto FetchItemInfo(int itemId)
        {
            Items item = _itemRepository.GetItemById(itemId);
            IDictionary<string, List<ItemPhotoDto>> photos = GetItemPhotosByItemId(item.Id);
            ItemDto itemDto = new ItemDto
            {
                Id = item.Id,
                Name = item.Name,
                Photos = photos
            };
            return itemDto;
        }

        public bool InsertNewPhoto(InsertPhotoDto toInsert)
        {
            ItemPhotos photoEntity = new ItemPhotos
            {
                CreatedAt = DateTime.Now,
                IsActive = true,
                ItemId = toInsert.ItemId,
                Position = toInsert.Photo.Position,
                FileName = toInsert.Photo.ImageName,
                TypeId = 1
            };

            bool photoInserted = _itemRepository.InsertPhoto(photoEntity);
            if (photoInserted)
            {
                List<ItemPhotoPropertySet> PropsToAdd = new List<ItemPhotoPropertySet>(0);
                ItemPhotoPropertySet photoMetalEntity = new ItemPhotoPropertySet
                {
                    ItemPhotoId = photoEntity.Id,
                    PropertyId = 1,
                    Value = toInsert.Metal
                };
                PropsToAdd.Add(photoMetalEntity);
                ItemPhotoPropertySet photoShapeEntity = new ItemPhotoPropertySet
                {
                    ItemPhotoId = photoEntity.Id,
                    PropertyId = 2,
                    Value = toInsert.Shape
                };
                PropsToAdd.Add(photoShapeEntity);
                return _itemRepository.InsertPhotoProperties(PropsToAdd);

            }
            return false;
        }

        private string GetPhotoProperties(int photoId)
        {
            var props = _itemRepository.GetPhotoPropertiesByPhotoId(photoId);
            return string.Format("{0};{1}", props.FirstOrDefault(x => x.PropertyId == 1).Value, props.FirstOrDefault(x => x.PropertyId == 2).Value);
        }
    }
}
