﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Service.Model.DB
{
    public partial class TypePropertySet
    {
        public int Id { get; set; }
        public int MediaTypeId { get; set; }
        public int PropertyId { get; set; }

        [ForeignKey("MediaTypeId")]
        [InverseProperty("TypePropertySet")]
        public Types MediaType { get; set; }
        [ForeignKey("PropertyId")]
        [InverseProperty("TypePropertySet")]
        public Properties Property { get; set; }
    }
}
