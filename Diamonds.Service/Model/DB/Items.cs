﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Service.Model.DB
{
    public partial class Items
    {
        public Items()
        {
            ItemPhotos = new HashSet<ItemPhotos>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [InverseProperty("Item")]
        public ICollection<ItemPhotos> ItemPhotos { get; set; }
    }
}
