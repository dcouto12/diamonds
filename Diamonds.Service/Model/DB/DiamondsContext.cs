﻿using Microsoft.EntityFrameworkCore;

namespace Diamonds.Service.Model.DB
{
    public partial class DiamondsContext : DbContext
    {
        public DiamondsContext()
        {
        }

        public DiamondsContext(DbContextOptions<DiamondsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ItemPhotoPropertySet> ItemPhotoPropertySet { get; set; }
        public virtual DbSet<ItemPhotos> ItemPhotos { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<Properties> Properties { get; set; }
        public virtual DbSet<TypePropertySet> TypePropertySet { get; set; }
        public virtual DbSet<Types> Types { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=localhost;Database=Diamonds;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemPhotoPropertySet>(entity =>
            {
                entity.HasOne(d => d.ItemPhoto)
                    .WithMany(p => p.ItemPhotoPropertySet)
                    .HasForeignKey(d => d.ItemPhotoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemPhotoPropertySet_ItemPhotos");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.ItemPhotoPropertySet)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemPhotoPropertySet_ItemPhotoPropertySet");
            });

            modelBuilder.Entity<ItemPhotos>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.ItemPhotos)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_ItemPhotos_Items");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.ItemPhotos)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ItemPhotos_Types");
            });

            modelBuilder.Entity<Properties>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<TypePropertySet>(entity =>
            {
                entity.HasOne(d => d.MediaType)
                    .WithMany(p => p.TypePropertySet)
                    .HasForeignKey(d => d.MediaTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TypePropertySet_Types");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.TypePropertySet)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TypePropertySet_Properties");
            });

            modelBuilder.Entity<Types>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });
        }
    }
}
