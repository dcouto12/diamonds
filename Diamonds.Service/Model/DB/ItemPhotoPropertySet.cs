﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Service.Model.DB
{
    public partial class ItemPhotoPropertySet
    {
        public int Id { get; set; }
        public int ItemPhotoId { get; set; }
        public int PropertyId { get; set; }
        [Required]
        [StringLength(50)]
        public string Value { get; set; }

        [ForeignKey("ItemPhotoId")]
        [InverseProperty("ItemPhotoPropertySet")]
        public ItemPhotos ItemPhoto { get; set; }
        [ForeignKey("PropertyId")]
        [InverseProperty("ItemPhotoPropertySet")]
        public Properties Property { get; set; }
    }
}
