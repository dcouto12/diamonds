﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Service.Model.DB
{
    public partial class Properties
    {
        public Properties()
        {
            ItemPhotoPropertySet = new HashSet<ItemPhotoPropertySet>();
            TypePropertySet = new HashSet<TypePropertySet>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [InverseProperty("Property")]
        public ICollection<ItemPhotoPropertySet> ItemPhotoPropertySet { get; set; }
        [InverseProperty("Property")]
        public ICollection<TypePropertySet> TypePropertySet { get; set; }
    }
}
