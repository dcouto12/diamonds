﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Service.Model.DB
{
    public partial class ItemPhotos
    {
        public ItemPhotos()
        {
            ItemPhotoPropertySet = new HashSet<ItemPhotoPropertySet>();
        }

        public int Id { get; set; }
        public int? ItemId { get; set; }
        public int TypeId { get; set; }
        [StringLength(50)]
        public string FileName { get; set; }
        public int? Position { get; set; }
        [StringLength(500)]
        public string Alt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedAt { get; set; }
        [Required]
        public bool? IsActive { get; set; }

        [ForeignKey("ItemId")]
        [InverseProperty("ItemPhotos")]
        public Items Item { get; set; }
        [ForeignKey("TypeId")]
        [InverseProperty("ItemPhotos")]
        public Types Type { get; set; }
        [InverseProperty("ItemPhoto")]
        public ICollection<ItemPhotoPropertySet> ItemPhotoPropertySet { get; set; }
    }
}
