﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Diamonds.Service.Model.DB
{
    public partial class Types
    {
        public Types()
        {
            ItemPhotos = new HashSet<ItemPhotos>();
            TypePropertySet = new HashSet<TypePropertySet>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [InverseProperty("Type")]
        public ICollection<ItemPhotos> ItemPhotos { get; set; }
        [InverseProperty("MediaType")]
        public ICollection<TypePropertySet> TypePropertySet { get; set; }
    }
}
