﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamonds.Service.Model.Dto
{
    public class ItemPropertyDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
