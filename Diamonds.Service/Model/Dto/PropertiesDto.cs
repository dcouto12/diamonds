﻿
namespace Diamonds.Service.Model.Dto
{
    public class PropertiesDto
    {
        public string Metal { get; set; }
        public string Shape { get; set; }
    }
}
