﻿using System.Collections.Generic;

namespace Diamonds.Service.Model.Dto
{
    public class ItemPhotoDto
    {
        public string ImageName { get; set; }
        public string ImageAlt { get; set; }
        public int Position { get; set; }
        public string Type { get; set; }
    }
}