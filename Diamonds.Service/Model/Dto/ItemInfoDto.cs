﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamonds.Service.Model.Dto
{
    public class ItemInfoDto
    {
        public string ItemName { get; set; }
        public int ItemId { get; set; }
    }

}
