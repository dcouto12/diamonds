﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamonds.Service.Model.Dto
{
    public class InsertPhotoDto
    {
        public int ItemId { get; set; }
        public string Metal { get; set; }
        public string Shape { get; set; }
        public ItemPhotoDto Photo { get; set; }
    }
}
