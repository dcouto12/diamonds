﻿using System.Collections.Generic;

namespace Diamonds.Service.Model.Dto
{
    public class ItemDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public IDictionary<string, List<ItemPhotoDto>> Photos { get; set; }
    }
}
