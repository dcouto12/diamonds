﻿using Diamonds.Service.Model.DB;
using System.Collections.Generic;
using System.Linq;

namespace Diamonds.Service.Repository
{
    public class ItemRepository
    {
        private readonly DiamondsContext _context;

        public ItemRepository(DiamondsContext context)
        {
            _context = context;
        }

        public IEnumerable<Items> GetItems()
        {
            IQueryable<Items> items = from item in _context.Items
                                      select item;
            return items.ToList();
        }

        public Items GetItemById(int itemId)
        {
            IQueryable<Items> items = from item in _context.Items
                                      where item.Id == itemId
                                      select item;
            return items.FirstOrDefault();
        }

        public IEnumerable<ItemPhotos> GetItemPhotosByItemId(int itemId)
        {
            IQueryable<ItemPhotos> photos = from itemPhoto in _context.ItemPhotos
                                            where itemPhoto.ItemId == itemId && itemPhoto.IsActive == true
                                            select itemPhoto;
            return photos.ToList();
        }

        public IEnumerable<ItemPhotoPropertySet> GetPhotoPropertiesByPhotoId(int photoId)
        {
            IQueryable<ItemPhotoPropertySet> query = from photoProps in _context.ItemPhotoPropertySet
                                                     where photoProps.ItemPhotoId == photoId
                                                     select photoProps;
            return query.ToList();
        }

        public bool InsertPhoto(ItemPhotos toInsert)
        {
            _context.ItemPhotos.Add(toInsert);
            try
            {
                return _context.SaveChanges() > 0;
            }
            catch
            {
                return false;
            }
        }

        public bool PhotoNameUnique(string photoName)
        {
            var query = from photos in _context.ItemPhotos
                        where photos.FileName.Equals(photoName)
                        select photos;

            return query.Any();
        }

        public bool InsertPhotoProperties(List<ItemPhotoPropertySet> propertiesToInsert)
        {
            _context.ItemPhotoPropertySet.AddRange(propertiesToInsert);
            try
            {
                return _context.SaveChanges() > 0;
            }
            catch
            {
                return false;
            }
        }

    }
}
