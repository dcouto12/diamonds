﻿using System.Collections.Generic;
using Diamonds.Service.Model.Dto;
using Diamonds.Service.Service;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Diamonds.Service.Controller
{
    [Route("api/[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly ItemService _service;

        public ItemController(ItemService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<ItemDto> Get()
        {
            return _service.FetchItemsInfo();
        }

        [HttpGet("{id}")]
        public ItemDto GetItemById(int id)
        {
            return _service.FetchItemInfo(id);
        }

        [HttpPost]
        public bool InsertPhoto(InsertPhotoDto newPhoto)
        {
            return _service.InsertNewPhoto(newPhoto);
        }

        [HttpGet("photoNameUnique")]
        public bool PhotoNameUnique(string photoName)
        {
            return _service.IsPhotoNameUnique(photoName);
        }
    }
}
