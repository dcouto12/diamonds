﻿
namespace Diamonds.Web.Models
{
    public class UploadDto
    {
        public int ItemId { get; set; }
        public string Material { get; set; }
        public string Shape { get; set; }
    }
}
