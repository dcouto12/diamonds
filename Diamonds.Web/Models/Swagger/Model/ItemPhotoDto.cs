using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class ItemPhotoDto {
    /// <summary>
    /// Gets or Sets ImageName
    /// </summary>
    [DataMember(Name="ImageName", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ImageName")]
    public string ImageName { get; set; }

    /// <summary>
    /// Gets or Sets ImageAlt
    /// </summary>
    [DataMember(Name="ImageAlt", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "ImageAlt")]
    public string ImageAlt { get; set; }

    /// <summary>
    /// Gets or Sets Position
    /// </summary>
    [DataMember(Name="Position", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Position")]
    public int? Position { get; set; }

    /// <summary>
    /// Gets or Sets Type
    /// </summary>
    [DataMember(Name="Type", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "Type")]
    public string Type { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class ItemPhotoDto {\n");
      sb.Append("  ImageName: ").Append(ImageName).Append("\n");
      sb.Append("  ImageAlt: ").Append(ImageAlt).Append("\n");
      sb.Append("  Position: ").Append(Position).Append("\n");
      sb.Append("  Type: ").Append(Type).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
