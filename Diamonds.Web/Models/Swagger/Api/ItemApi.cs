using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IItemApi
    {
        /// <summary>
        ///  
        /// </summary>
        /// <returns>List&lt;ItemDto&gt;</returns>
        List<ItemDto> ApiItemGet ();
        /// <summary>
        ///  
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ItemDto</returns>
        ItemDto ApiItemIdGet (int? id);
        /// <summary>
        ///  
        /// </summary>
        /// <param name="photoName"></param>
        /// <returns>bool?</returns>
        bool? ApiItemPhotoNameUniqueGet (string photoName);
        /// <summary>
        ///  
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="metal"></param>
        /// <param name="shape"></param>
        /// <param name="photoImageName"></param>
        /// <param name="photoImageAlt"></param>
        /// <param name="photoPosition"></param>
        /// <param name="photoType"></param>
        /// <returns>bool?</returns>
        bool? ApiItemPost (int? itemId, string metal, string shape, string photoImageName, string photoImageAlt, int? photoPosition, string photoType);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class ItemApi : IItemApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public ItemApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemApi"/> class.
        /// </summary>
        /// <returns></returns>
        public ItemApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        ///  
        /// </summary>
        /// <returns>List&lt;ItemDto&gt;</returns>
        public List<ItemDto> ApiItemGet ()
        {
    
            var path = "/api/Item";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                                                    
            // authentication setting, if any
            String[] authSettings = new String[] {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (List<ItemDto>) ApiClient.Deserialize(response.Content, typeof(List<ItemDto>), response.Headers);
        }
    
        /// <summary>
        ///  
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ItemDto</returns>
        public ItemDto ApiItemIdGet (int? id)
        {
            // verify the required parameter 'id' is set
            if (id == null) throw new ApiException(400, "Missing required parameter 'id' when calling ApiItemIdGet");
    
            var path = "/api/Item/{id}";
            path = path.Replace("{format}", "json");
            path = path.Replace("{" + "id" + "}", ApiClient.ParameterToString(id));
    
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                                                    
            // authentication setting, if any
            String[] authSettings = new String[] {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemIdGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemIdGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (ItemDto) ApiClient.Deserialize(response.Content, typeof(ItemDto), response.Headers);
        }
    
        /// <summary>
        ///  
        /// </summary>
        /// <param name="photoName"></param>
        /// <returns>bool?</returns>
        public bool? ApiItemPhotoNameUniqueGet (string photoName)
        {
    
            var path = "/api/Item/photoNameUnique";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (photoName != null) queryParams.Add("photoName", ApiClient.ParameterToString(photoName)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemPhotoNameUniqueGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemPhotoNameUniqueGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (bool?) ApiClient.Deserialize(response.Content, typeof(bool?), response.Headers);
        }
    
        /// <summary>
        ///  
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="metal"></param>
        /// <param name="shape"></param>
        /// <param name="photoImageName"></param>
        /// <param name="photoImageAlt"></param>
        /// <param name="photoPosition"></param>
        /// <param name="photoType"></param>
        /// <returns>bool?</returns>
        public bool? ApiItemPost (int? itemId, string metal, string shape, string photoImageName, string photoImageAlt, int? photoPosition, string photoType)
        {
    
            var path = "/api/Item";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (itemId != null) queryParams.Add("ItemId", ApiClient.ParameterToString(itemId)); // query parameter
 if (metal != null) queryParams.Add("Metal", ApiClient.ParameterToString(metal)); // query parameter
 if (shape != null) queryParams.Add("Shape", ApiClient.ParameterToString(shape)); // query parameter
 if (photoImageName != null) queryParams.Add("Photo.ImageName", ApiClient.ParameterToString(photoImageName)); // query parameter
 if (photoImageAlt != null) queryParams.Add("Photo.ImageAlt", ApiClient.ParameterToString(photoImageAlt)); // query parameter
 if (photoPosition != null) queryParams.Add("Photo.Position", ApiClient.ParameterToString(photoPosition)); // query parameter
 if (photoType != null) queryParams.Add("Photo.Type", ApiClient.ParameterToString(photoType)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemPost: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ApiItemPost: " + response.ErrorMessage, response.ErrorMessage);
    
            return (bool?) ApiClient.Deserialize(response.Content, typeof(bool?), response.Headers);
        }
    
    }
}
