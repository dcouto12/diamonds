﻿using IO.Swagger.Model;
using System.Collections.Generic;

namespace Diamonds.Web.Models
{
    public class EditModel
    {
        public EditModel(ItemModel item)
        {
            ItemId = item.IdITem;
            ItemName = item.Name;
            Photos = new List<PhotoModel>(0);
        }

        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public List<PhotoModel> Photos { get; set; }

    }

    public class PhotoModel
    {
        public int PhotoId { get; set; }
        public int Position { get; set; }
        public string MetalProperty { get; set; }
        public string ShapeProperty { get; set; }
        public string Thumbnail { get; set; }
        public List<ImageModel> ImagePath { get; set; }
    }

    public class ImageModel
    {
        public string ImagePath { get; set; }
        public string ImageAlt { get; set; }
    }
}
