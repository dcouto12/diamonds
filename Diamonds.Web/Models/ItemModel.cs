﻿using IO.Swagger.Model;
using System.Collections.Generic;
using System.Linq;

namespace Diamonds.Web.Models
{
    public class ItemModel
    {
        public int IdITem { get; set; }
        public string Name { get; set; }
        public IDictionary<string,List<ItemPhotoDto>> Photos { get; set; }


        public ItemModel(ItemDto dto)
        {
            IdITem = dto.Id.Value;
            Name = dto.Name;
            Photos = dto.Photos;
        }
    }
}
