﻿using Diamonds.Web.Models;
using IO.Swagger.Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Diamonds.Web.Controllers
{
    public class ItemsController : Controller
    {
        private readonly ItemApi _service;
        private readonly IHostingEnvironment _host;

        public ItemsController(ItemApi service, IHostingEnvironment host)
        {
            _service = service;
            _host = host;
        }

        public IActionResult EditItem(int itemId)
        {
            ItemModel item = new ItemModel(_service.ApiItemIdGet(itemId));
            string[] metals, shapes;
            bool hasPhotos = false;
            if (item.Photos.Keys.Count > 0)
            {
                metals = item.Photos.Keys.Select(s => s.Split(';')[0]).Distinct().ToArray();
                shapes = item.Photos.Keys.Select(s => s.Split(';')[1]).Distinct().ToArray();
                ViewBag.ColumnWidth = 12 / (metals.Count() + 1);
                hasPhotos = true;
                ViewBag.Metals = metals;
                ViewBag.Shapes = shapes;
            }
            ViewBag.HasPhotos = hasPhotos;
            return View(item);
        }

        [HttpPost]
        public IActionResult UploadFile(int itemId, string material, string shape, IFormFile file)
        {
            bool updated = _service.ApiItemPost(itemId, material, shape, file.FileName, "", 0, "PHOTO").Value;

            if (updated && file.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    string ImagesPath = Path.Combine(_host.WebRootPath, "images\\ring", file.FileName);
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    try
                    {
                        System.IO.File.WriteAllBytes(ImagesPath, fileBytes);
                        //Add notification Upload succeeded
                    }
                    catch
                    {
                        //Add Notification upload failed
                        return RedirectToAction("Index", "Home", new { message = "Error Uploading photo" });
                    }
                }
            }
            return RedirectToAction("Index", "Home", new { message = "Photo Upload Successfully"});
        }
    }
}
