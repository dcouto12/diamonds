﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Diamonds.Web.Models;
using IO.Swagger.Api;
using System.Collections.Generic;
using System.Linq;

namespace Diamonds.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ItemApi _service;

        public HomeController(ItemApi service)
        {
            _service = service;
        }

        public IActionResult Index(string message = null)
        {
            var items = _service.ApiItemGet();
            IList<ItemModel> models = items.Select(i => new ItemModel(i)).ToList();
            bool withMessage = false;
            if (!string.IsNullOrEmpty(message))
            {
                withMessage = true;
                ViewBag.Message = message;
            }
            ViewBag.WithMessage = withMessage;
            return View(models);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
